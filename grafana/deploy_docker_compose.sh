#!/bin/bash
set +e

#Install commands
if ! command -v docker &> /dev/null
then
	sudo apt-get update
	sudo apt-get install ca-certificates curl gnupg -y
	sudo install -m 0755 -d /etc/apt/keyrings
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
	sudo chmod a+r /etc/apt/keyrings/docker.gpg
	echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
	sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	sudo apt-get update
	sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
	sudo usermod -aG docker $USER
	newgrp docker
fi

if ! command -v pip3 &> /dev/null
then
	sudo apt-get update
	sudo apt-get install python3 python3-pip -y
fi

if ! command -v docker-compose &> /dev/null
then
	sudo apt-get update
	sudo pip3 install docker-compose
fi


#Start Grafana
docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
docker-compose pull grafana
docker-compose up --quiet-pull --detach --force-recreate --remove-orphans grafana
set -e
