# Serenko Test Task

## Installation Ubuntu 22.04

1. Login Gitlab.com
2. Go to your project’s Settings > CI/CD and expand the Variables section.
3. Select Add variable and fill:

| Variable | Description |
| ------ | ------ |
| SSH_PRIVATE_KEY | https://docs.gitlab.com/ee/ci/ssh_keys/ Private Keys Server |
| DEV_USER | Username Server |
| DEV_HOST | Ip-address Server |

4. On the left sidebar, select CI/CD > Pipelines.
5. Select Run pipeline.
6. Go to the pipeline, job, environment, or deployment view.
7. Next to the manual job of Deploy, select Play.
8. Login to the interface Grafana  "Ip-address Server:3000" (Login/password : admin/admin).
9. On the left sidebar, select Dashboard and search nginx tag.



# Kubernetes command:
Being in the kubernetes folder run the following commands:

	kubectl apply -f frontend
	kubectl apply -f nginx-exporter
	kubectl apply -f prometheus
	kubectl apply -f grafana
	
Files secrets.yaml needs to be filled with a access token https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/